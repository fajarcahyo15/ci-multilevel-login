<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // cek apakah pengguna yang mengakses url telah login
        $this->auth->cek_auth();
    }

    function index() {
        $this->load->view('home');
    }

    public function superadmin() {
        $this->auth->hak_akses('SUPERADMIN');
        $this->session->set_flashdata('message','Berhasil eksekusi menu SUPERADMIN');
        redirect('home');
    }

    public function admin() {
        $this->auth->hak_akses('ADMIN');
        $this->session->set_flashdata('message','Berhasil eksekusi menu ADMIN');
        redirect('home');
    }

    public function pegawai() {
        $this->auth->hak_akses('PEGAWAI');
        $this->session->set_flashdata('message','Berhasil eksekusi menu PEGAWAI');
        redirect('home');
    }

    public function pengunjung() {
        $this->auth->hak_akses('PENGUNJUNG');
        $this->session->set_flashdata('message','Berhasil eksekusi menu PENGUNJUNG');
        redirect('home');
    }




}
