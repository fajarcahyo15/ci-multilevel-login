<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

    public function __construct() {
        parent::__construct();
        //Codeigniter : Write Less Do More
        $this->load->model('LoginModel');
    }

    function index() {
        $session = $this->session->userdata('isLogin');
        if($session == FALSE) {
            $this->load->view('login');
        } else if($session == TRUE) {
            redirect('Home');
        }
    }

    // method proses login admin
    public function login() {
        // mendapatkan username dan password dari form login
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $cek = $this->LoginModel->cek_user($username,$password);
        // cek jika data ada pada database
        if(count($cek) == 1) {

            // Mendapatkan informasi admin
            foreach ($cek as $admin) {
                $level = $admin['level'];
                $username = $admin['username'];
            }

            // Menyimpan informasi yang didapat ke dalam session
            $this->session->set_userdata(array(
                'isLogin'   => TRUE,
                'username'  => $username,
                'level'      => $level,
            ));

            // Mengarahkan admin ke halaman Home Admin
            redirect('home','refresh');

            // Jika username dan password tidak ditemukan
        } else {
            $this->session->set_flashdata('errMessage','Username atau Password salah !');
            redirect('admin');
        }
    }

    // method logout admin
    public function logout() {
        $data = array('isLogin','username','level');
        $this->session->unset_userdata($data);
        redirect('admin','refresh');
    }

}
