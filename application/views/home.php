<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Admin Home</title>
    </head>
    <body>
        <h2>Halaman Admin</h2>
        
        <!-- Menampilkan informasi user -->
        Username: <?= $this->session->userdata('username') ?> | Level: <?= $this->session->userdata('level') ?><br>

        <!-- Menampilkan pesan error jika menu yang dipilih tidak sesuai hak akses -->
        <div style="color: red;"> <?= $this->session->flashdata('errMessage') ?> </div>

        <!-- Menampilkan informasi jika menu yang dipilih sesuai hak akses -->
        <?= $this->session->flashdata('message') ?>

        <!-- Daftar menu -->
        <ul>
            <li> <a href="<?= base_url('home/superadmin') ?>">MENU SUPERADMIN</a> </li>
            <li> <a href="<?= base_url('home/admin') ?>">MENU ADMIN</a> </li>
            <li> <a href="<?= base_url('home/pegawai') ?>">MENU PEGAWAI</a> </li>
            <li> <a href="<?= base_url('home/pengunjung') ?>">MENU PENGUNJUNG</a> </li>
            <li> <a href="<?= base_url('admin/logout') ?>">LOGOUT</a> </li>
        </ul>
    </body>
</html>
