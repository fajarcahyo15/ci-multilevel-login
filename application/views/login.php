<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Halaman Login</title>
    </head>
    <body>
        <h2>Login Admin</h2>
        <form action="<?= base_url('admin/login'); ?>" method="post" role="form">
            <?= $this->session->flashdata('errMessage') ?>
            <fieldset>
                <label for="username">Username</label><br>
                <input type="text" name="username" id="txt_username" placeholder="Masukkan Username" required>
                <br><br>
                <label for="password">Password</label><br>
                <input type="password" name="password" id="txt_password" placeholder="Masukkan Password" required>
                <br><br>
                <input type="submit" name="submit" id="txt_submit" value="Login">
                <input type="reset" name="reset" value="Reset">
            </fieldset>
        </form>
    </body>
</html>
