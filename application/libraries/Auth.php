<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth {

    public function cek_auth() {

        // menginstansiasi method bawaan ci kedalam variabel $this->ci
        $this->ci =& get_instance();

        // mendapatkan session admin
        $this->sesiadmin  = $this->ci->session->userdata('isLogin');
        $this->hak = $this->ci->session->userdata('level');

        // cek jika session admin bernilai false maka kembalikan user ke halaman login
        if($this->sesiadmin != TRUE){
			$this->ci->session->set_flashdata('errMessage', 'Silahkan Login Terlebih Dahulu!');
			redirect('Admin','refresh');
			exit();
		}
    }

    public function hak_akses($user="") {

        // cek apakah halaman yang diakses sesuai dengan hak akses yang diberikan
        if($this->hak<>$user) {
    		$this->ci->session->set_flashdata('errMessage', 'Anda tidak berhak mengakses halaman ini!');
    		redirect('Home');

        // Jika tidak ditemukan hak akses maka kembalikan user ke halaman login
    	} else if($this->hak=="") {
    		$this->ci->session->set_flashdata('errMessage', 'Anda belum login!');
    		redirect('Admin');
    	}
    }
}


?>
