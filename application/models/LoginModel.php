<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model{

    public function __construct() {
        parent::__construct();
        $this->tabel = 'login';
    }

    function cek_user($username,$password) {
		$query = $this->db->get_where($this->tabel, array('username' => $username, 'password' => md5($password)));
		$query = $query->result_array();
		return $query;
	}

    function ambil_user($username) {
        $query = $this->db->get_where($this->tabel, array('username' => $username));
        $query = $query->result_array();
        if($query) {
            return $query[0];
        }
    }



}
